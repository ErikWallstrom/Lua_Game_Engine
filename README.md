#Lua Game Engine V.0.1 Alpha

About:
	This is a game engine written from scratch using C (C11). It is intended
	to be used from Lua scripts, and has excellent performance under both
	Windows and Linux. 

Lua script API Documentation: https://trello.com/b/MzBbLDb4/lua-game-engine-ish

Usage (windows):

	1. Go to build/win32
	2. Create a new file called `script.lua`
	3. Paste this in the file and save: `Game:create_scene():set_color(255, 0, 0)`
	4. Double click on `start_game.exe`
	5. If you see a window with red background, you are good to go. Check Lua script API documentation for more information (stated above)

Usage (linux):
Make sure you have these dependencies: 

	*SDL2 
	*SDL2_image
	*SDL2_ttf 
	*SDL2_net 
	*GLEW 
	*GL
	*lua5.3

	1. `cd <path_to_lua_game_engine>/build/linux`
	2. `touch script.lua`
	3. Paste this in the file and save: `Game:create_scene():set_color(255, 0, 0)`
	4. `sudo chmod 777 start_game`
	5. `sudo chmod 777 start_game.sh`
	6. `./start_game.sh`
	7. If you see a window with red background, you are good to go. Check Lua script API documentation for more information (stated above)

**Please check the example folder for advanced usage.**
