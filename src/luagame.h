#ifndef LUAGAME_H
#define LUAGAME_H

#include "API/game.h"
#include <lua.h>

#define LUAGAME_NAME "Game"

void luagame_init(lua_State* L, struct Game* self);

#endif
