#include "luagame.h"
#include "luafont.h"
#include "luascene.h"
#include "gamestate.h"
#include "luatexture.h"
#include "API/log.h"
#include "API/dobjecthandler.h"
#include <stdlib.h>
#include <lauxlib.h>
#include <SDL2/SDL.h>

int luagame_createscene(lua_State* L);
int luagame_loadimage(lua_State* L);
int luagame_loadfont(lua_State* L);
int luagame_rendertext(lua_State* L);
int luagame_keydown(lua_State* L); 
int luagame_mousedown(lua_State* L); 
int luagame_getmousex(lua_State* L);
int luagame_getmousey(lua_State* L);
int luagame_getwidth(lua_State* L);
int luagame_getheight(lua_State* L);
int luagame_setscene(lua_State* L);
int luagame_getticks(lua_State* L);
int luagame_quit(lua_State* L);

const struct luaL_Reg luagame_methods[] = {
	{"create_scene", luagame_createscene},
	{"load_image", luagame_loadimage},
	{"load_font", luagame_loadfont},
	{"render_text", luagame_rendertext},
	{"key_down", luagame_keydown},
	{"mouse_down", luagame_mousedown},
	{"get_mouse_x", luagame_getmousex},
	{"get_mouse_y", luagame_getmousey},
	{"get_width", luagame_getwidth},
	{"get_height", luagame_getheight},
	{"set_scene", luagame_setscene},
	{"get_ticks", luagame_getticks},
	{"quit", luagame_quit},
	{NULL, NULL}
};

void luagame_init(lua_State* L, struct Game* self)
{
	log_assert(L, "is NULL");
	log_assert(self, "is NULL");

	luaL_newmetatable(L, LUAGAME_NAME);
	lua_pushvalue(L, -1);
	lua_setfield(L, -2, "__index");
	luaL_setfuncs(L, luagame_methods, 0);
	lua_pop(L, 1);

	lua_pushlightuserdata(L, self);
	luaL_getmetatable(L, LUAGAME_NAME);
	lua_setmetatable(L, -2);
	lua_setglobal(L, "Game");
}

int luagame_createscene(lua_State* L)
{
	struct Game* self = luaL_checkudata(L, 1, LUAGAME_NAME);
	struct Scene* scene = lua_newuserdata(L, sizeof(struct Scene));
	luaL_getmetatable(L, LUASCENE_NAME);
	lua_setmetatable(L, -2);

	struct GameState* state = self->userdata;
	struct LuaScene* luascene = malloc(sizeof(struct LuaScene));

	luascene->r = 255;
	luascene->g = 255;
	luascene->b = 255;
	luascene->keydownid = 0;
	luascene->keyupid = 0;
	luascene->mousedownid = 0;
	luascene->mouseupid = 0;
	luascene->updateid = 0;
	luascene->L = L;

	dobjecthandler_ctor(
		&luascene->dobjhandler, 
		state->window.renderer, 
		scene, 
		NULL
	);

	lua_pushvalue(L, -1);
	luascene->self = luaL_ref(L, LUA_REGISTRYINDEX);

	scene_ctor(scene, luascene_update, luascene_render, luascene);
	game_add(self, scene);
	return 1;
}

int luagame_loadimage(lua_State* L)
{
	struct Game* self = luaL_checkudata(L, 1, LUAGAME_NAME);
	const char* file = luaL_checkstring(L, 2);

	struct GameState* state = self->userdata;
	struct Texture* texture = lua_newuserdata(L, sizeof(struct Texture));
	luaL_getmetatable(L, LUATEXTURE_NAME);
	lua_setmetatable(L, -2);

	imageloader_load(&state->imageloader, texture, file);
	return 1;
}

int luagame_loadfont(lua_State* L)
{
	struct Game* self = luaL_checkudata(L, 1, LUAGAME_NAME);
	const char* file = luaL_checkstring(L, 2);
	int size = luaL_checkinteger(L, 3);

	struct GameState* state = self->userdata;
	FontID* font = lua_newuserdata(L, sizeof(FontID));
	luaL_getmetatable(L, LUAFONT_NAME);
	lua_setmetatable(L, -2);

	FontID id = fontloader_load(&state->fontloader, file, size);
	*font = id;
	return 1;
}

int luagame_rendertext(lua_State* L)
{
	struct Game* self = luaL_checkudata(L, 1, LUAGAME_NAME);
	FontID* font = luaL_checkudata(L, 2, LUAFONT_NAME);
	const char* text = luaL_checkstring(L, 3);

	struct GameState* state = self->userdata;
	struct Texture* texture = lua_newuserdata(L, sizeof(struct Texture));
	luaL_getmetatable(L, LUATEXTURE_NAME);
	lua_setmetatable(L, -2);

	fontloader_render(
		&state->fontloader, 
		texture, 
		text, 
		*font, 
		FONTQUALITY_HIGH //?
	);

	return 1;
}

int luagame_keydown(lua_State* L)
{
	struct Game* self = luaL_checkudata(L, 1, LUAGAME_NAME);
	const char* key = luaL_checkstring(L, 2);

	SDL_Scancode code = SDL_GetScancodeFromName(key);
	char* errmsgstatic = " is not a valid key";
	size_t errmsglen = strlen(errmsgstatic) + strlen(key);
	Vec(char) errmsg = vec_ctor(char, errmsglen + 1);
	strcpy(errmsg, key);
	strcpy(errmsg + strlen(key), errmsgstatic);

	luaL_argcheck(L, code != SDL_SCANCODE_UNKNOWN, 2, errmsg);
	vec_dtor(errmsg);

	struct GameState* state = self->userdata;
	lua_pushboolean(L, state->window.keystate[code]);
	return 1;
}

int luagame_mousedown(lua_State* L)
{
	struct Game* self = luaL_checkudata(L, 1, LUAGAME_NAME);
	struct GameState* state = self->userdata;
	const char* button = luaL_checkstring(L, 2);

	if(!strcmp(button, "left"))
	{
		lua_pushboolean(
			L, 
			state->window.mousestate &
				SDL_BUTTON(SDL_BUTTON_LEFT)
		);
	}
	else if(!strcmp(button, "right"))
	{
		lua_pushboolean(
			L, 
			state->window.mousestate &
				SDL_BUTTON(SDL_BUTTON_RIGHT)
		);
	}
	else if(!strcmp(button, "middle"))
	{
		lua_pushboolean(
			L, 
			state->window.mousestate &
				SDL_BUTTON(SDL_BUTTON_MIDDLE)
		);
	}
	else
		luaL_argerror(L, 2, "invalid mouse button");

	return 1;
}

int luagame_getmousex(lua_State* L)
{
	struct Game* self = luaL_checkudata(L, 1, LUAGAME_NAME);
	struct GameState* state = self->userdata;
	lua_pushinteger(L, state->window.mousex);
	return 1;
}

int luagame_getmousey(lua_State* L)
{
	struct Game* self = luaL_checkudata(L, 1, LUAGAME_NAME);
	struct GameState* state = self->userdata;
	lua_pushinteger(L, state->window.mousey);
	return 1;
}

int luagame_getwidth(lua_State* L)
{
	struct Game* self = luaL_checkudata(L, 1, LUAGAME_NAME);
	struct GameState* state = self->userdata;
	lua_pushinteger(L, state->window.width);
	return 1;
}

int luagame_getheight(lua_State* L)
{
	struct Game* self = luaL_checkudata(L, 1, LUAGAME_NAME);
	struct GameState* state = self->userdata;
	lua_pushinteger(L, state->window.height);
	return 1;
}

int luagame_setscene(lua_State* L)
{
	struct Game* self = luaL_checkudata(L, 1, LUAGAME_NAME);
	struct Scene* scene = luaL_checkudata(L, 2, LUASCENE_NAME);

	for(size_t i = 0; i < vec_getsize(self->scenes); i++)
	{
		if(self->scenes[i] == scene)
		{
			self->selectedscene = i;
			break;
		}
	}
	return 0;
}

int luagame_getticks(lua_State* L)
{
	struct Game* self = luaL_checkudata(L, 1, LUAGAME_NAME);
	lua_pushinteger(L, self->totalticks);
	return 1;
}

int luagame_quit(lua_State* L)
{
	struct Game* self = luaL_checkudata(L, 1, LUAGAME_NAME);
	self->done = 1;
	return 0;
}
