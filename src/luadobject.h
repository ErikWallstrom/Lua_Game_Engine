#ifndef LUADOBJECT_H
#define LUADOBJECT_H

#include "API/dobject.h"
#include <lua.h>

#define LUADOBJECT_NAME "DObject"

struct LuaDObject
{
	int behavior;
	int custom;
	int texture;
	int self;
	lua_State* L;
};

void luadobject_behavior(
	struct DObject* self, 
	struct Scene* scene, 
	void* userdata
);
void luadobject_init(lua_State* L);

#endif
