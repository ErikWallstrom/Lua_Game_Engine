#include "luafont.h"
#include "API/log.h"
#include <lauxlib.h>

const struct luaL_Reg luafont_methods[] = {
	{NULL, NULL}
};

void luafont_init(lua_State* L)
{
	log_assert(L, "is NULl");

	luaL_newmetatable(L, LUAFONT_NAME);
	lua_pushvalue(L, -1);
	lua_setfield(L, -2, "__index");
	luaL_setfuncs(L, luafont_methods, 0);
	lua_pop(L, 1);
}

