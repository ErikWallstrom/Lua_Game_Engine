#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "API/imageloader.h"
#include "API/fontloader.h"
#include "API/window.h"

#include <lua.h>

struct GameState
{
	struct Window window;
	struct FontLoader fontloader;
	struct ImageLoader imageloader;

	lua_State* L;
};

#endif
