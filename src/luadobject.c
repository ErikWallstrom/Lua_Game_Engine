#include "luadobject.h"
#include "luatexture.h"
#include "API/dobject.h"
#include "API/log.h"
#include <lauxlib.h>
#include <stdlib.h>

int luadobject_movex(lua_State* L);
int luadobject_movey(lua_State* L);
int luadobject_getx(lua_State* L);
int luadobject_gety(lua_State* L);
int luadobject_setwidth(lua_State* L);
int luadobject_setheight(lua_State* L);
int luadobject_getwidth(lua_State* L);
int luadobject_getheight(lua_State* L);
int luadobject_rotate(lua_State* L);
int luadobject_getrotation(lua_State* L);
int luadobject_flipx(lua_State* L);
int luadobject_flipy(lua_State* L);
int luadobject_isflippedx(lua_State* L);
int luadobject_isflippedy(lua_State* L);
int luadobject_setbehavior(lua_State* L);
int luadobject_getbehavior(lua_State* L);
//int luadobject_setrpoint(lua_State* L);
//int luadobject_getrpoint(lua_State* L);
//int luadobject_getsrect(lua_State* L);
int luadobject_setsrect(lua_State* L);
int luadobject_intersects(lua_State* L);
int luadobject_hitleft(lua_State* L);
int luadobject_hitright(lua_State* L);
int luadobject_hittop(lua_State* L);
int luadobject_hitbottom(lua_State* L);
int luadobject_gettexture(lua_State* L);
int luadobject_settexture(lua_State* L);
int luadobject_getcustom(lua_State* L);
int luadobject_setcustom(lua_State* L);
int luadobject_dtor(lua_State* L);

const struct luaL_Reg luadobject_methods[] = {
	{"move_x", luadobject_movex},
	{"move_y", luadobject_movey},
	{"get_x", luadobject_getx},
	{"get_y", luadobject_gety},
	{"set_width", luadobject_setwidth},
	{"set_height", luadobject_setheight},
	{"get_width", luadobject_getwidth},
	{"get_height", luadobject_getheight},
	{"rotate", luadobject_rotate},
	{"get_rotation", luadobject_getrotation},
	{"flip_x", luadobject_flipx},
	{"flip_y", luadobject_flipy},
	{"is_flipped_x", luadobject_isflippedx},
	{"is_flipped_y", luadobject_isflippedy},
	{"set_behavior", luadobject_setbehavior},
	{"get_behavior", luadobject_getbehavior},
	{"set_srect", luadobject_setsrect},
	{"intersects", luadobject_intersects},
	{"hit_left", luadobject_hitleft},
	{"hit_right", luadobject_hitright},
	{"hit_top", luadobject_hittop},
	{"hit_bottom", luadobject_hitbottom},
	{"set_texture", luadobject_settexture},
	{"get_texture", luadobject_gettexture},
	{"set_custom", luadobject_setcustom},
	{"get_custom", luadobject_getcustom},
	{"__gc", luadobject_dtor},
	{NULL, NULL}
};

void luadobject_behavior(
	struct DObject* self, 
	struct Scene* scene, 
	void* userdata
)
{
	dobject_defaultbehavior(self, scene, userdata);

	struct LuaDObject* luadobj = self->userdata;
	if(luadobj->behavior)
	{
		lua_pushinteger(luadobj->L, luadobj->behavior);
		lua_gettable(luadobj->L, LUA_REGISTRYINDEX);
		lua_pushinteger(luadobj->L, luadobj->self);
		lua_gettable(luadobj->L, LUA_REGISTRYINDEX);
		lua_pushinteger(luadobj->L, luadobj->custom);
		lua_gettable(luadobj->L, LUA_REGISTRYINDEX);

		if(lua_pcall(luadobj->L, 2, 0, 0))
		{
			log_error(lua_tostring(luadobj->L, -1));
		}
	}
}

int luadobject_index(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	struct LuaDObject* luadobj = self->userdata;

	luaL_getmetatable(L, LUADOBJECT_NAME);
	lua_pushvalue(L, 2);
	lua_rawget(L, -2);

	if(lua_isnil(L, -1) && luadobj->custom)
	{
		lua_rawgeti(L, LUA_REGISTRYINDEX, luadobj->custom);
		if(lua_istable(L, -1))
		{
			lua_pushvalue(L, 2);
			lua_gettable(L, -2);
		}
	}

	return 1;
}

void luadobject_init(lua_State* L)
{
	log_assert(L, "is NULL");
	luaL_newmetatable(L, LUADOBJECT_NAME);
	lua_pushvalue(L, -1);

	lua_pushliteral(L, "__index");
	lua_pushcfunction(L, luadobject_index);
	lua_rawset(L, -3);

	luaL_setfuncs(L, luadobject_methods, 0);
	lua_pop(L, 1);
}

int luadobject_movex(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	double x = luaL_checknumber(L, 2);

	self->changex += x;
	return 0;
}

int luadobject_movey(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	double y = luaL_checknumber(L, 2);

	self->changey += y;
	return 0;
}

int luadobject_getx(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	//struct DObjectPos pos = dobject_getrealpos(self);
	//lua_pushnumber(L, pos.x);
	lua_pushnumber(L, self->x);
	return 1;
}

int luadobject_gety(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	//struct DObjectPos pos = dobject_getrealpos(self);
	//lua_pushnumber(L, pos.y);
	lua_pushnumber(L, self->y);
	return 1;
}

int luadobject_setwidth(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	int width = luaL_checkinteger(L, 2);
	self->w = width;
	return 0;
}

int luadobject_setheight(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	int height = luaL_checkinteger(L, 2);
	self->h = height;
	return 0;
}

int luadobject_getwidth(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	lua_pushinteger(L, self->w);
	return 1;
}

int luadobject_getheight(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	lua_pushinteger(L, self->h);
	return 1;
}

int luadobject_rotate(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	double rotation = luaL_checknumber(L, 2);
	self->rotation += rotation;
	return 0;
}

int luadobject_getrotation(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	lua_pushnumber(L, self->rotation);
	return 1;
}

int luadobject_flipx(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	self->flipx = !self->flipx;
	return 0;
}

int luadobject_flipy(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	self->flipy = !self->flipy;
	return 0;
}

int luadobject_isflippedx(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	lua_pushboolean(L, self->flipx);
	return 1;
}

int luadobject_isflippedy(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	lua_pushboolean(L, self->flipy);
	return 1;
}

int luadobject_setbehavior(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);

	luaL_checktype(L, 2, LUA_TFUNCTION);
	int behavior = luaL_ref(L, LUA_REGISTRYINDEX);
	struct LuaDObject* ldobject = self->userdata;
	if(ldobject->behavior)
		luaL_unref(L, LUA_REGISTRYINDEX, ldobject->behavior);
	ldobject->behavior = behavior;
	return 0;
}

int luadobject_getbehavior(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	struct LuaDObject* ldobject = self->userdata;
	
	if(ldobject->behavior)
		lua_rawgeti(L, LUA_REGISTRYINDEX, ldobject->behavior);
	else
		lua_pushnil(L);
	return 1;
}

int luadobject_setsrect(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);

	luaL_checktype(L, 2, LUA_TTABLE);
	lua_len(L, 2);
	int len = lua_tointeger(L, -1);
	luaL_argcheck(L, len == 4, 2, "table must contain 4 integers");

	int values[4];
	for(int i = 0; i < 4; i++)
	{
		lua_pushinteger(L, i + 1);
		lua_gettable(L, 2);
		values[i] = luaL_checkinteger(L, -1);
	}

	self->srect.x = values[0];
	self->srect.y = values[1];
	self->srect.w = values[2];
	self->srect.h = values[3];
	return 0;
}

int luadobject_intersects(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);

	if(lua_isuserdata(L, 2))
	{
		struct DObject* dobj = luaL_checkudata(L, 2, LUADOBJECT_NAME);
		if(dobject_intersects(self, dobj))
		{
			lua_pushboolean(L, 1);
		}
		else
		{
			lua_pushboolean(L, 0);
		}
	}
	else if(lua_isnumber(L, 2) && lua_isnumber(L, 3))
	{
		double x = lua_tonumber(L, 2);
		double y = lua_tonumber(L, 3);
		if(dobject_intersectspoint(self, x, y))
		{
			lua_pushboolean(L, 1);
		}
		else
		{
			lua_pushboolean(L, 0);
		}
	}
	else
	{
		luaL_argerror(L, 2, "invalid argument");
	}

	return 1;
}

int luadobject_hitleft(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	struct DObject* dobj = luaL_checkudata(L, 2, LUADOBJECT_NAME);

	if(dobject_hittestleft(self, dobj))
		lua_pushboolean(L, 1);
	else
		lua_pushboolean(L, 0);
	return 1;
}

int luadobject_hitright(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	struct DObject* dobj = luaL_checkudata(L, 2, LUADOBJECT_NAME);

	if(dobject_hittestright(self, dobj))
		lua_pushboolean(L, 1);
	else
		lua_pushboolean(L, 0);
	return 1;
}

int luadobject_hittop(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	struct DObject* dobj = luaL_checkudata(L, 2, LUADOBJECT_NAME);

	if(dobject_hittesttop(self, dobj))
		lua_pushboolean(L, 1);
	else
		lua_pushboolean(L, 0);
	return 1;
}

int luadobject_hitbottom(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	struct DObject* dobj = luaL_checkudata(L, 2, LUADOBJECT_NAME);

	if(dobject_hittestbottom(self, dobj))
		lua_pushboolean(L, 1);
	else
		lua_pushboolean(L, 0);
	return 1;
}

int luadobject_gettexture(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	struct LuaDObject* luadobj = self->userdata;

	lua_rawgeti(L, LUA_REGISTRYINDEX, luadobj->texture);
	return 1;
}

int luadobject_settexture(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	struct Texture* texture = luaL_checkudata(L, 2, LUATEXTURE_NAME);
	struct LuaDObject* luadobj = self->userdata;

	luaL_unref(L, LUA_REGISTRYINDEX, luadobj->texture);
	self->texture = texture;
	luadobj->texture = luaL_ref(L, LUA_REGISTRYINDEX);
	return 0;
}

int luadobject_getcustom(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	struct LuaDObject* luadobj = self->userdata;

	if(luadobj->custom)
		lua_rawgeti(L, LUA_REGISTRYINDEX, luadobj->custom);
	else
		lua_pushnil(L);
	return 1;
}

int luadobject_setcustom(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	struct LuaDObject* luadobj = self->userdata;

	if(luadobj->custom)
		luaL_unref(L, LUA_REGISTRYINDEX, luadobj->custom);

	if(!lua_isnil(L, 2))
		luadobj->custom = luaL_ref(L, LUA_REGISTRYINDEX);
	else
		luadobj->custom = 0;

	return 0;
}

int luadobject_dtor(lua_State* L)
{
	struct DObject* self = luaL_checkudata(L, 1, LUADOBJECT_NAME);
	struct LuaDObject* luadobj = self->userdata;

	luaL_unref(luadobj->L, LUA_REGISTRYINDEX, luadobj->behavior);
	luaL_unref(luadobj->L, LUA_REGISTRYINDEX, luadobj->custom);
	luaL_unref(luadobj->L, LUA_REGISTRYINDEX, luadobj->self);

	free(luadobj);
	return 0;
}

