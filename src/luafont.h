#ifndef LUAFONT_H
#define LUAFONT_H

#include <lua.h>

#define LUAFONT_NAME "Font"

void luafont_init(lua_State* L);

#endif
