#ifndef LUATEXTURE_H
#define LUATEXTURE_H

#include <lua.h>

#define LUATEXTURE_NAME "Texture"

void luatexture_init(lua_State* L);

#endif
