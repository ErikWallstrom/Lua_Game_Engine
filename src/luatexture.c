#include "luatexture.h"
#include "API/texture.h"
#include "API/log.h"
#include <lauxlib.h>

int luatexture_setcolor(lua_State* L);
int luatexture_getwidth(lua_State* L);
int luatexture_getheight(lua_State* L);
int luatexture_dtor(lua_State* L);

const struct luaL_Reg luatexture_methods[] = {
	{"set_color", luatexture_setcolor},
	{"get_width", luatexture_getwidth},
	{"get_height", luatexture_getheight},
	{"__gc", luatexture_dtor},
	{NULL, NULL}
};

void luatexture_init(lua_State* L)
{
	log_assert(L, "is NULL");
	luaL_newmetatable(L, LUATEXTURE_NAME);
	lua_pushvalue(L, -1);
	lua_setfield(L, -2, "__index");
	luaL_setfuncs(L, luatexture_methods, 0);
	lua_pop(L, 1);
}

int luatexture_setcolor(lua_State* L)
{
	struct Texture* self = luaL_checkudata(L, 1, LUATEXTURE_NAME);
	int r = luaL_checkinteger(L, 2);
	int g = luaL_checkinteger(L, 3);
	int b = luaL_checkinteger(L, 4);

	SDL_SetTextureColorMod(self->raw, r, g, b);
	return 0;
}

int luatexture_getwidth(lua_State* L)
{
	struct Texture* self = luaL_checkudata(L, 1, LUATEXTURE_NAME);
	lua_pushinteger(L, self->width);
	return 1;
}

int luatexture_getheight(lua_State* L)
{
	struct Texture* self = luaL_checkudata(L, 1, LUATEXTURE_NAME);
	lua_pushinteger(L, self->height);
	return 1;
}

int luatexture_dtor(lua_State* L)
{
	struct Texture* self = luaL_checkudata(L, 1, LUATEXTURE_NAME);
	SDL_DestroyTexture(self->raw);
	return 0;
}

