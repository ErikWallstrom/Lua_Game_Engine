#ifndef LUASCENE_H
#define LUASCENE_H

#include "API/dobjecthandler.h"
#include <lua.h>

struct Game;
struct Scene;

#define LUASCENE_NAME "Scene"

struct LuaScene
{
	struct DObjectHandler dobjhandler;
	int r, g, b;
	int keydownid;
	int keyupid;
	int mousedownid;
	int mouseupid;
	int updateid;
	int self;

	lua_State* L;
};

void luascene_update(struct Scene* scene, struct Game* game);
void luascene_render(
	struct Scene* scene, 
	struct Game* game, 
	double interpolation
);
void luascene_init(lua_State* L);

#endif
