#include "luascene.h"
#include "gamestate.h"
#include "luadobject.h"
#include "luatexture.h"
#include "API/dobject.h"
#include "API/scene.h"
#include "API/game.h"
#include "API/log.h"
#include "API/vec.h"
#include <lauxlib.h>
#include <stdlib.h>
#include <string.h>

int luascene_add(lua_State* L);
int luascene_remove(lua_State* L);
int luascene_change(lua_State* L);
int luascene_setcolor(lua_State* L);
int luascene_addeventlistener(lua_State* L);
int luascene_getcontent(lua_State* L);
int luascene_dtor(lua_State* L);

void luascene_update(struct Scene* scene, struct Game* game)
{
	struct LuaScene* luascene = scene->userdata;
	struct GameState* state = game->userdata;

	for(size_t i = 0; i < vec_getsize(state->window.events); i++)
	{
		switch(state->window.events[i].type)
		{
		case SDL_KEYDOWN:
			if(luascene->keydownid)
			{
				if(!state->window.events[i].key.repeat)
				{
					lua_pushinteger(luascene->L, luascene->keydownid);
					lua_gettable(luascene->L, LUA_REGISTRYINDEX);
					lua_pushinteger(luascene->L, luascene->self);
					lua_gettable(luascene->L, LUA_REGISTRYINDEX);
					lua_pushstring(
						luascene->L, 
						SDL_GetScancodeName(
							state->window.events[i].key.keysym.scancode
						)
					);

					if(lua_pcall(luascene->L, 2, 0, 0))
					{
						log_error(lua_tostring(luascene->L, -1));
					}
				}
			}
			break;
		case SDL_KEYUP:
			if(luascene->keyupid)
			{
				lua_pushinteger(luascene->L, luascene->keyupid);
				lua_gettable(luascene->L, LUA_REGISTRYINDEX);
				lua_pushinteger(luascene->L, luascene->self);
				lua_gettable(luascene->L, LUA_REGISTRYINDEX);
				lua_pushstring(
					luascene->L, 
					SDL_GetScancodeName(
						state->window.events[i].key.keysym.scancode
					)
				);

				if(lua_pcall(luascene->L, 2, 0, 0))
				{
					log_error(lua_tostring(luascene->L, -1));
				}
			}
			break;
		case SDL_MOUSEBUTTONDOWN:
			if(luascene->mousedownid)
			{
				lua_pushinteger(
					luascene->L, 
					luascene->mousedownid
				);
				lua_gettable(luascene->L, LUA_REGISTRYINDEX);
				lua_pushinteger(luascene->L, luascene->self);
				lua_gettable(luascene->L, LUA_REGISTRYINDEX);
				lua_pushinteger(luascene->L, state->window.mousex);
				lua_pushinteger(luascene->L, state->window.mousey);

				if(lua_pcall(luascene->L, 3, 0, 0))
				{
					log_error(lua_tostring(luascene->L, -1));
				}
			}
			break;
		case SDL_MOUSEBUTTONUP:
			if(luascene->mouseupid)
			{
				lua_pushinteger(luascene->L, luascene->mouseupid);
				lua_gettable(luascene->L, LUA_REGISTRYINDEX);
				lua_pushinteger(luascene->L, luascene->self);
				lua_gettable(luascene->L, LUA_REGISTRYINDEX);
				lua_pushinteger(luascene->L, state->window.mousex);
				lua_pushinteger(luascene->L, state->window.mousey);

				if(lua_pcall(luascene->L, 3, 0, 0))
				{
					log_error(lua_tostring(luascene->L, -1));
				}
			}
			break;
		default:;
		}
	}

	if(luascene->updateid)
	{
		lua_pushinteger(luascene->L, luascene->updateid);
		lua_gettable(luascene->L, LUA_REGISTRYINDEX);
		lua_pushinteger(luascene->L, luascene->self);
		lua_gettable(luascene->L, LUA_REGISTRYINDEX);

		if(lua_pcall(luascene->L, 1, 0, 0))
		{
			log_error(lua_tostring(luascene->L, -1));
		}
	}

	dobjecthandler_update(&luascene->dobjhandler);
	state->window.read = 1;

	static Uint32 oldtime = 0;
	if(SDL_GetTicks() / 1000 > oldtime)
	{
		oldtime = SDL_GetTicks() / 1000;
		log_info(
			"FPS: %i, Objects: %zu", 
			state->window.fps, 
			vec_getsize(luascene->dobjhandler.dobjects)
		);
	}
}

void luascene_render(
	struct Scene* scene, 
	struct Game* game, 
	double interpolation
)
{
	struct LuaScene* luascene = scene->userdata;
	struct GameState* state = game->userdata;

	dobjecthandler_render(&luascene->dobjhandler, interpolation);
	SDL_SetRenderDrawColor(
		state->window.renderer, 
		luascene->r, 
		luascene->g, 
		luascene->b, 
		255
	);
	game->done = !window_update(&state->window);
}

const struct luaL_Reg luascene_methods[] = {
	{"add", luascene_add},
	{"remove", luascene_remove},
	{"change", luascene_change},
	{"set_color", luascene_setcolor},
	{"add_event_listener", luascene_addeventlistener},
	{"get_content", luascene_getcontent},
	{"__gc", luascene_dtor},
	{NULL, NULL}
};

void luascene_init(lua_State* L)
{
	log_assert(L, "is NULL");

	luaL_newmetatable(L, LUASCENE_NAME);
	lua_pushvalue(L, -1);
	lua_setfield(L, -2, "__index");
	luaL_setfuncs(L, luascene_methods, 0);
	lua_pop(L, 1);
}

int luascene_add(lua_State* L)
{
	struct Scene* self = luaL_checkudata(L, 1, LUASCENE_NAME);
	luaL_checktype(L, 2, LUA_TTABLE);

	struct LuaScene* luascene = self->userdata;
	struct DObject* dobj = lua_newuserdata(L, sizeof(struct DObject));
	luaL_getmetatable(L, LUADOBJECT_NAME);
	lua_setmetatable(L, -2);
	
	struct Texture* texture = NULL;
	switch(lua_getfield(L, 2, "texture"))
	{
	case LUA_TUSERDATA:;
		texture = luaL_checkudata(L, -1, LUATEXTURE_NAME);
		break;
	default:
		luaL_checkudata(L, -1, "Texture");
	}
	int textureid = luaL_ref(L, LUA_REGISTRYINDEX);

	double x = 0.0;
	switch(lua_getfield(L, 2, "x"))
	{
	case LUA_TNUMBER:
		x = lua_tonumber(L, -1);
		break;
	default:
		luaL_checktype(L, -1, LUA_TNUMBER);
	}
	lua_pop(L, 1);

	double y = 0.0;
	switch(lua_getfield(L, 2, "y"))
	{
	case LUA_TNUMBER:
		y = lua_tonumber(L, -1);
		break;
	default:
		luaL_checktype(L, -1, LUA_TNUMBER);
	}
	lua_pop(L, 1);

	enum DObjectRegPoint rpoint = DOBJECTREG_TOPLEFT;
	switch(lua_getfield(L, 2, "rpoint"))
	{
	case LUA_TSTRING:;
		const char* str = lua_tostring(L, -1);
		if(!strcmp(str, "top_left"))
			rpoint = DOBJECTREG_TOPLEFT;
		else if(!strcmp(str, "top_right"))
			rpoint = DOBJECTREG_TOPRIGHT;
		else if(!strcmp(str, "bottom_left"))
			rpoint = DOBJECTREG_BOTTOMLEFT;
		else if(!strcmp(str, "bottom_right"))
			rpoint = DOBJECTREG_BOTTOMRIGHT;
		else if(!strcmp(str, "center"))
			rpoint = DOBJECTREG_CENTER;
		else if(!strcmp(str, "center_left"))
			rpoint = DOBJECTREG_CENTERLEFT;
		else if(!strcmp(str, "center_right"))
			rpoint = DOBJECTREG_CENTERRIGHT;
		else if(!strcmp(str, "center_top"))
			rpoint = DOBJECTREG_CENTERTOP;
		else if(!strcmp(str, "center_bottom"))
			rpoint = DOBJECTREG_CENTERBOTTOM;
		else
		{
			char* errmsgstatic = " is not a valid rpoint";
			size_t errmsglen = strlen(errmsgstatic) + strlen(str);
			Vec(char) errmsg = vec_ctor(char, errmsglen + 1);
			strcpy(errmsg, str);
			strcpy(errmsg + strlen(str), errmsgstatic);

			luaL_argerror(L, 1, errmsg);
			vec_dtor(errmsg);
		}
	case LUA_TNIL:
		break;
	default:
		luaL_checktype(L, -1, LUA_TSTRING);
	}
	lua_pop(L, 1);

	struct LuaDObject* luadobj = malloc(sizeof(struct LuaDObject));
	
	int behavior = 0;
	switch(lua_getfield(L, 2, "behavior"))
	{
	case LUA_TFUNCTION:
		behavior = luaL_ref(L, LUA_REGISTRYINDEX);
		break;
	case LUA_TNIL:
		lua_pop(L, 1);
		break;
	default:
		luaL_checktype(L, -1, LUA_TFUNCTION);
	}

	int custom = 0;
	switch(lua_getfield(L, 2, "custom"))
	{
	case LUA_TNIL:
		lua_pop(L, 1);
		break;
	default:
		custom = luaL_ref(L, LUA_REGISTRYINDEX);
	}

	luadobj->behavior = behavior;
	luadobj->custom = custom;
	luadobj->L = L;
	luadobj->texture = textureid;

	lua_pushvalue(L, -1);
	luadobj->self = luaL_ref(L, LUA_REGISTRYINDEX);

	dobject_ctor(
		dobj, 
		texture, 
		x, 
		y, 
		rpoint, 
		luadobject_behavior, 
		dobject_defaultrender, 
		luadobj
	);

	switch(lua_getfield(L, 2, "srect"))
	{
	case LUA_TTABLE:;
		int srect[4] = {0};
		for(int i = 0; i < 4; i++)
		{
			lua_pushinteger(L, i + 1);
			lua_gettable(L, -2);
			srect[i] = luaL_checkinteger(L, -1);
			lua_pop(L, 1);
		}
		dobj->srect.x = srect[0];
		dobj->srect.y = srect[1];
		dobj->srect.w = srect[2];
		dobj->srect.h = srect[3];

	case LUA_TNIL:
		break;
	default:
		luaL_checktype(L, -1, LUA_TTABLE);
	}
	lua_pop(L, 1);

	switch(lua_getfield(L, 2, "width"))
	{
	case LUA_TNUMBER:
		dobj->w = lua_tointeger(L, -1);
		break;
	case LUA_TNIL: 
		break;
	default:
		luaL_checktype(L, -1, LUA_TNUMBER);
	}
	lua_pop(L, 1);

	switch(lua_getfield(L, 2, "height"))
	{
	case LUA_TNUMBER:
		dobj->h = lua_tointeger(L, -1);
		break;
	case LUA_TNIL: 
		break;
	default:
		luaL_checktype(L, -1, LUA_TNUMBER);
	}
	lua_pop(L, 1);

	switch(lua_getfield(L, 2, "rotation"))
	{
	case LUA_TNUMBER:
		dobj->rotation = lua_tonumber(L, -1);
	case LUA_TNIL: 
		break;
	default:
		luaL_checktype(L, -1, LUA_TNUMBER);
	}
	lua_pop(L, 1);

	switch(lua_getfield(L, 2, "flip_x"))
	{
	case LUA_TBOOLEAN:
		dobj->flipx = lua_toboolean(L, -1);
	case LUA_TNIL: 
		break;
	default:
		luaL_checktype(L, -1, LUA_TBOOLEAN);
	}
	lua_pop(L, 1);

	switch(lua_getfield(L, 2, "flip_y"))
	{
	case LUA_TBOOLEAN:
		dobj->flipy = lua_toboolean(L, -1);
	case LUA_TNIL: 
		break;
	default:
		luaL_checktype(L, -1, LUA_TBOOLEAN);
	}
	lua_pop(L, 1);
	dobjecthandler_add(&luascene->dobjhandler, dobj);
	return 1;
}

int luascene_remove(lua_State* L)
{
	struct Scene* self = luaL_checkudata(L, 1, LUASCENE_NAME);
	struct DObject* dobject = luaL_checkudata(L, 2, LUADOBJECT_NAME);

	struct LuaScene* luascene = self->userdata;
	dobjecthandler_remove(&luascene->dobjhandler, dobject);
	return 0;
}

int luascene_change(lua_State* L)
{
	struct Scene* self = luaL_checkudata(L, 1, LUASCENE_NAME);
	const char* type = luaL_checkstring(L, 2);

	if(!strcmp(type, "next"))
		self->change = SCENECHANGE_NEXT;
	else if(!strcmp(type, "prev"))
		self->change = SCENECHANGE_PREV;
	else
	{
		char* errmsgstatic = " is not a valid change";
		size_t errmsglen = strlen(errmsgstatic) + strlen(type);
		Vec(char) errmsg = vec_ctor(char, errmsglen + 1);
		strcpy(errmsg, type);
		strcpy(errmsg + strlen(type), errmsgstatic);

		luaL_argerror(L, 1, errmsg);
		vec_dtor(errmsg);
	}

	return 0;
}

int luascene_setcolor(lua_State* L)
{
	struct Scene* self = luaL_checkudata(L, 1, LUASCENE_NAME);
	int r = luaL_checkinteger(L, 2);
	int g = luaL_checkinteger(L, 3);
	int b = luaL_checkinteger(L, 4);

	struct LuaScene* luascene = self->userdata;
	luascene->r = r;
	luascene->g = g;
	luascene->b = b;

	return 0;
}

int luascene_addeventlistener(lua_State* L)
{
	struct Scene* self = luaL_checkudata(L, 1, LUASCENE_NAME);
	const char* type = luaL_checkstring(L, 2);
	luaL_checktype(L, 3, LUA_TFUNCTION);
	int listenerid = luaL_ref(L, LUA_REGISTRYINDEX);

	struct LuaScene* luascene = self->userdata;

	if(!strcmp(type, "key_up"))
		luascene->keyupid = listenerid;
	else if(!strcmp(type, "key_down"))
		luascene->keydownid = listenerid;
	else if(!strcmp(type, "mouse_up"))
		luascene->mouseupid = listenerid;
	else if(!strcmp(type, "mouse_down"))
		luascene->mousedownid = listenerid;
	else if(!strcmp(type, "update"))
		luascene->updateid = listenerid;
	else
	{
		char* errmsgstatic = " is not a valid event";
		size_t errmsglen = strlen(errmsgstatic) + strlen(type);
		Vec(char) errmsg = vec_ctor(char, errmsglen + 1);
		strcpy(errmsg, type);
		strcpy(errmsg + strlen(type), errmsgstatic);
		luaL_argerror(L, 1, errmsg);
		vec_dtor(errmsg);
	}

	return 0;
}

int luascene_getcontent(lua_State* L)
{
	struct Scene* self = luaL_checkudata(L, 1, LUASCENE_NAME);
	struct LuaScene* luascene = self->userdata;

	lua_newtable(L);
	int tableidx = lua_gettop(L);

	for(size_t i = 0; i < vec_getsize(luascene->dobjhandler.dobjects); i++)
	{
		struct DObject* dobject = luascene->dobjhandler.dobjects[i];
		if(dobject)
		{
			lua_pushinteger(L, i + 1);
			struct LuaDObject* luadobject = dobject->userdata;
			lua_rawgeti(L, LUA_REGISTRYINDEX, luadobject->self);
			lua_settable(L, tableidx);
		}
	}

	return 1;
}

int luascene_dtor(lua_State* L)
{
	struct Scene* self = luaL_checkudata(L, 1, LUASCENE_NAME);
	struct LuaScene* luascene = self->userdata;

	dobjecthandler_dtor(&luascene->dobjhandler);
	luaL_unref(luascene->L, LUA_REGISTRYINDEX, luascene->self);
	if(luascene->keydownid)
		luaL_unref(luascene->L, LUA_REGISTRYINDEX, luascene->keydownid);
	if(luascene->keyupid)
		luaL_unref(luascene->L, LUA_REGISTRYINDEX, luascene->keyupid);
	if(luascene->mousedownid)
		luaL_unref(luascene->L, LUA_REGISTRYINDEX, luascene->mousedownid);
	if(luascene->mouseupid)
		luaL_unref(luascene->L, LUA_REGISTRYINDEX, luascene->mouseupid);
	if(luascene->updateid)
		luaL_unref(luascene->L, LUA_REGISTRYINDEX, luascene->updateid);
	free(luascene);

	return 0;
}

