#include "API/argparser.h"
#include "API/log.h"

#include "luadobject.h"
#include "luatexture.h"
#include "gamestate.h"
#include "luascene.h"
#include "luagame.h"
#include "luafont.h"

#include <lauxlib.h>
#include <stdlib.h>
#include <lualib.h>

void handleoptions(
	int argc, 
	char* argv[], 
	const char** scriptfile, 
	int* windowflags
)
{
	struct ArgParserOption options[] = {
		{'h', "help", "Get this message", 0},
		{'v', "version", "Get version number", 0},
		{'s', "script", "Set the script file that should be used", 0},
		{ 0 , "disable-vsync", "Can improve framerate or reduce input lag", 0},
	};

	struct ArgParserResult result = argparser_parseoptions(
		argc, 
		argv, 
		options,
		sizeof options / sizeof *options
	);

	if(result.invalidoptions || 
		(options[2].used) ? (vec_getsize(result.args) != 1) : 
			vec_getsize(result.args) > 0
	)
	{
		argparser_printhelp(
			argv[0], 
			options, 
			sizeof options / sizeof *options
		);
		exit(EXIT_FAILURE);
	}

	if(options[0].used)
	{
		argparser_printhelp(
			argv[0], 
			options, 
			sizeof options / sizeof *options
		);
		exit(EXIT_SUCCESS);
	}

	if(options[1].used)
	{
		puts("Version: Alpha build 0.1\n");
		exit(EXIT_SUCCESS);
	}

	if(options[2].used)
	{
		*scriptfile = result.args[0];
	}

	if(options[3].used)
	{
		*windowflags = WINDOW_RENDERER;
	}

	vec_dtor(result.args);
}

#undef main

int main(int argc, char* argv[])
{
	const char* scriptfile = "script.lua";
	int windowflags = WINDOW_RENDERER | WINDOW_VSYNC;

	handleoptions(argc, argv, &scriptfile, &windowflags);

	struct GameState state;
	struct Game game;
	game_ctor(&game, 60, &state);

	window_init();
	window_ctor(&state.window, "Game Window", 800, 600, windowflags);
	fontloader_ctor(&state.fontloader, state.window.renderer);
	imageloader_ctor(&state.imageloader, state.window.renderer);

	state.L = luaL_newstate();

	luaL_openlibs(state.L);
	luagame_init(state.L, &game);
	luafont_init(state.L);
	luascene_init(state.L);
	luatexture_init(state.L);
	luadobject_init(state.L);

	if(luaL_dofile(state.L, scriptfile))
	{
		log_error(lua_tostring(state.L, -1));
	}

	game_start(&game);
	lua_close(state.L);

	imageloader_dtor(&state.imageloader);
	fontloader_dtor(&state.fontloader);
	window_dtor(&state.window);
	game_dtor(&game);
}

