local texture = Game:load_image("./Test.jpg")
local scene = Game:create_scene()
scene:add_event_listener("mouse_down", function(self, x, y)
	self:add {
		texture = texture,
		x = x,
		y = y,
		width = 50,
		height = 50,
		rpoint = "center",
		behavior = function(self)
			self:rotate(1)
		end
	}
end)
