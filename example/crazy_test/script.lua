function test_behavior(self, custom)
	self:move_x(math.cos(math.rad(self:get_rotation())) * custom.speed)
	self:move_y(math.sin(math.rad(self:get_rotation())) * custom.speed)

	self:rotate(1)
end

function effect(self, custom)
	self:move_x(math.cos(math.rad(self:get_rotation())) * custom.speed)
	self:move_y(math.sin(math.rad(self:get_rotation())) * custom.speed)

	self:rotate(1)
	self:get_texture():set_color(
		math.random(255), 
		math.random(255), 
		math.random(255)
	)

	if self:intersects(keemstar) then
		scene:remove(self)
	end
end

function do_cool_effect()
	for i = 1, 5 do
		scene:add {
			texture = effect_texture,
			x = keemstar:get_x() - keemstar:get_width(),
			y = keemstar:get_y(),
			rpoint = "center",
			width = 60,
			height = 60,
			rotation = math.random(360),
			behavior = effect, 
			custom = {speed = 2, texture = effect_texture}
		}
	end
end

function move_left_right(self, custom)
	if Game:key_down('s') then
		self:rotate(5)
	end
	if Game:key_down('w') then
		self:rotate(-5)
	end

	if Game:key_down('d') then
		self:move_x(math.cos(math.rad(self:get_rotation())) * custom.speed)
		self:move_y(math.sin(math.rad(self:get_rotation())) * custom.speed)
	end

	if Game:key_down('a') then
		self:move_x(-math.cos(math.rad(self:get_rotation())) * custom.speed)
		self:move_y(-math.sin(math.rad(self:get_rotation())) * custom.speed)
	end

	if Game:key_down('space') then
		do_cool_effect()
	end

	self:hit_right(test)
	self:hit_left(test)
	self:hit_top(test)
	self:hit_bottom(test)
end

function move_left_right2(self, custom)
	if Game:key_down('j') then
		self:rotate(1)
	end
	if Game:key_down('k') then
		self:rotate(-1)
	end

	if Game:key_down('l') then
		self:move_x(math.cos(math.rad(self:get_rotation())) * custom.speed)
		self:move_y(math.sin(math.rad(self:get_rotation())) * custom.speed)
	end

	if Game:key_down('h') then
		self:move_x(-math.cos(math.rad(self:get_rotation())) * custom.speed)
		self:move_y(-math.sin(math.rad(self:get_rotation())) * custom.speed)
	end

	if self:intersects(
		Game:get_mouse_x(), 
		Game:get_mouse_y()) then
		self:get_texture():set_color(0, 0, 255)
	else
		self:get_texture():set_color(255, 0, 0)
	end
end

effect_texture = Game:load_image("./gnomestar.png")
gnome = Game:load_image("./gnomestar.png")

scene = Game:create_scene()
scene:add_event_listener("key_down", function(self, key)
	if key:lower() == "return" then
		self:change('next')
	end
end)
scene:add_event_listener("mouse_down", function(self, x, y)
	if keemstar:intersects(x, y) then
		keemstar:set_color(
			math.random(0, 255), 
			math.random(0, 255),
			math.random(0, 255)
		) 
	end
end)
scene:set_color(255, 255, 255)

test = scene:add {
	texture = gnome,
	x = 400,
	y = 300,
	rpoint = "center",
	width = 100,
	height = 200,
	behavior = move_left_right2,
	custom = {speed = 5}
}

keemstar = scene:add {
	texture = gnome,
	x = 0,
	y = 0,
	rpoint = "center",
	srect = {
		0, 
		0, 
		gnome:get_width() // 2, 
		gnome:get_height() // 2
	},
	width = 200,
	height = 200,
	behavior = move_left_right,
	custom = {speed = 5}
}

new_scene = Game:create_scene()
new_scene:add_event_listener("key_down", function(self, key)
	if key:lower() == "return" then
		self:change('prev')
	end
end)

tests = {}
for i = 1, 20 do
	tests[i] = new_scene:add {
		texture = effect_texture,
		x = math.random(0, 800),
		y = math.random(0, 600),
		rpoint = "center",
		width = 60,
		height = 60,
		rotation = math.random(360),
		behavior = test_behavior, 
		custom = {speed = 2, texture = effect_texture}
	}

	tests[i]:get_texture():set_color(255, 255, 255)
end

font = Game:load_font("./Roboto-Regular.ttf", 24)
new_scene:add {
	texture = Game:render_text(font, "Press enter to go back"),
	x = 400,
	y = 100,
	rpoint = "center"
}:get_texture():set_color(0, 0, 0)

scene:add {
	texture = Game:render_text(font, "Press space, move using WASD. Press enter to change scene"),
	x = 400,
	y = 100,
	rpoint = "center"
}:get_texture():set_color(0, 0, 0)


