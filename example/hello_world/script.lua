--Create main scene
local scene = Game:create_scene()

--Load a font with size 48
local font = Game:load_font("./Roboto-Regular.ttf", 48)

--Create texture from the font
local my_text = Game:render_text(font, "Hello world!")
--
--Set the text color
my_text:set_color(0, 0, 0)

--Add a new object to the scene
scene:add {
	texture = my_text,
	x = Game:get_width() / 2, --Middle of the screen
	y = Game:get_height() / 2, --Middle of the screen
	rpoint = "center"
}
