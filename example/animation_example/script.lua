local Sprite = require("sprite")

local font = Game:load_font("./Roboto-Regular.ttf", 24)
local img = Game:load_image("./Player.png")
local stand_animation = { 
	delay = 60,
	{0, 0, img:get_width() / 6, img:get_height()} 
}
local walk_animation = {
	delay = 6,
	{0, 0, img:get_width() / 6, img:get_height()},
	{40, 0, img:get_width() / 6, img:get_height()},
	{80, 0, img:get_width() / 6, img:get_height()},
	{120, 0, img:get_width() / 6, img:get_height()},
}

local scene = Game:create_scene()
scene:add_event_listener("update", function(self)
	if Game:mouse_down("left") then
		Sprite.new(
			self:add {
				texture = img, 
				x = math.random(Game:get_width()),
				y = math.random(Game:get_height()),
				width = img:get_width() / 6,
			},
			{stand_animation, walk_animation}
		)
	end
end)

scene:add_event_listener("key_down", function(self, key)
	if key == "Q" then
		Game:quit()
	elseif key == "Space" then
		img:set_color(
			math.random(255), 
			math.random(255), 
			math.random(255)
		)
	end
end)


test_text = scene:add {
	texture = Game:render_text(font, "Use WASD to move, Click to add new players"),
	x = Game:get_width() / 2,
	y = Game:get_height() / 2,
	rpoint = "center"
}

test_text:get_texture():set_color(0, 0, 0)
