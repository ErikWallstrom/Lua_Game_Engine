local Sprite = {}
Sprite.__index = Sprite

function Sprite:set_animation(selected)
	self:get_custom().selected_animation = selected
end

function Sprite:behavior()
	local moving = false
	if Game:key_down("W") then
		moving = true
		self:move_y(-self.speed)
		self:set_animation(2)
	end
	if Game:key_down("S") then
		moving = true
		self:move_y(self.speed)
		self:set_animation(2)
	end
	if Game:key_down("A") then
		moving = true
		self:move_x(-self.speed)
		self:set_animation(2)
		if not self:is_flipped_x() then
			self:flip_x()
		end
	end
	if Game:key_down("D") then
		moving = true
		self:move_x(self.speed)
		self:set_animation(2)
		if self:is_flipped_x() then
			self:flip_x()
		end
	end

	if not moving then
		self:set_animation(1)
	end

	if #self.animations > 0 then
		local animation = self.animations[self.selected_animation]
		if Game:get_ticks() - animation.delay > animation.time then
			animation.frame = animation.frame + 1
			if animation.frame > #animation then
				animation.frame = 1
			end

			animation.time = Game:get_ticks()
			self:set_srect(animation[animation.frame])
		end
	end
end

return {
	new = function(dobject, animations)
		local self = setmetatable({}, Sprite)
		local animations_copy = {}
		for i = 1, #animations do
			animations_copy[i] = {}
			for j = 1, #animations[i] do
				animations_copy[i].delay = animations[i].delay
				animations_copy[i].time = 0
				animations_copy[i].frame = 1
				animations_copy[i][j] = animations[i][j]
			end
		end

		self.animations = animations_copy
		self.selected_animation = 1
		self.speed = 5
		
		dobject:set_behavior(self.behavior)
		dobject:set_custom(self)
		return dobject
	end
}
