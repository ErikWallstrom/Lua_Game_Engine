local scene = Game:create_scene()
local font = Game:load_font("./Roboto-Regular.ttf", 48)
local my_text = Game:render_text(font, "Click me!")

my_text:set_color(0, 0, 0)
math.randomseed(os.time())

local button = scene:add {
	texture = my_text,
	x = Game:get_width() / 2, --Middle of the screen
	y = Game:get_height() / 2, --Middle of the screen
	rpoint = "center",
}

scene:add_event_listener("mouse_down", function(self, x, y)
	if button:intersects(x, y) then
		self:set_color(
			math.random(0, 255), 
			math.random(0, 255), 
			math.random(0, 255)
		)
	end
end)
