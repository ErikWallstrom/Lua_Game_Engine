function keyboard_behavior(self, custom)
	if Game:key_down("w") then
		self:move_y(-custom.speed)
	end
	if Game:key_down("s") then
		self:move_y(custom.speed)
	end
	if Game:key_down("a") then
		self:move_x(-custom.speed)
	end
	if Game:key_down("d") then
		self:move_x(custom.speed)
	end
end

local scene = Game:create_scene()
scene:add {
	texture = Game:load_image("./gnomestar.png"),
	x = Game:get_width() / 2,
	y = Game:get_height() / 2,
	rpoint = "center",
	width = 150,
	height = 280,
	behavior = keyboard_behavior,
	custom = {speed = 5}
}
